package bcdj;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class AtualizarCapProdDiariaMaq {
    String codMaquina="";
    int capProdDiaria=0;
    Scanner scanner = new Scanner(System.in);
    String[][] lista = new String[80][2];
    
    
    /*--
    Permite alterar a capacidade produtiva diária de uma máquina, dado o seu código.
    Não faria alterações.
    */
    public void codMaquinaCapDiaria() {
        
        int i = 0;
        String[] linha;
        boolean adp = true;
        try {
            Scanner in = new Scanner(new File("maquinas.txt"));
            while (in.hasNextLine() && i < 80) {
                linha = in.nextLine().split(",");
                lista[i][0] = linha[0];
                lista[i][1] = linha[1];
                i++;
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        }
        
        while (adp) {
        
        System.out.println("Digite o codigo da maquina que vai ser alterado:");
        codMaquina = scanner.next();
        System.out.println("");
        
        for (int j = 0; j < i; j++) {
            if (codMaquina.equals(lista[j][0])) {
                System.out.println("Digite a capacidade produtiva diária a inserir:");
                capProdDiaria = scanner.nextInt();
                adp = false;
                break;
            }
        }
        }
        
        for (int j = 0; j < i; j++) {
            if (codMaquina.equals(lista[j][0])) {
                lista[j][1] = Integer.toString(capProdDiaria);
            }
        }

            String eol = System.getProperty("line.separator");
            
            BufferedWriter bw = null;
            try {
                String[][] conteudo = new String[i][2];
                
                for (int k = 0; k < i; k++) {
                    for (int l = 0; l < 2; l++) {
                        conteudo[k][l] = lista[k][l];
                    }
                }
                
                //Nome do caminho e arquivo (Neste caso colocamos na pasta do programa)
                File file = new File("maquinas.txt");

                /* Cria o ficheiro caso o ficheiro não exista*/
                if (!file.exists()) {
                    file.createNewFile();
                }
            // Vai escrever no ficheiro
            FileWriter fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            for (int k = 0; k < i; k++) {
                for (int l = 0; l < 1; l++)
                    bw.write(conteudo[k][l] + "," + conteudo[k][l+1] + "\r\n");
            }
                System.out.println("");
            System.out.println("Capacidade Produtiva Diaria da maquina atualizada com sucesso!");

            } catch (IOException ioe) {
               ioe.printStackTrace();
            }
            finally
            { 
               try{
                  if(bw!=null)
                     bw.close();
               }catch(Exception ex){
                   System.out.println("Erro ao atualizar"+ex);
                }
            }
            System.exit(0);
        
            
        
        
    }
}
