
package bcdj;

import java.util.Scanner;
import java.io.*;
public class Bcdj {

    /**
     * @param args the command line arguments
     */
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws FileNotFoundException,IOException {
        char opcao;
        boolean verificar = false;

        Maquina ver = new Maquina();
        ver.lerFicheiro();

        ProdutividadeSemanal semana = new ProdutividadeSemanal();
        do {
            System.out.println("Menu");
            System.out.println("a) Mostrar no ecra a informacao base de uma determinada maquina.");
            System.out.println("b) Atualizar a capacidade produtiva diaria de uma maquina.");
            System.out.println("c) Remover informacao de maquina removida da linha de producao.");
            System.out.println("d) Guardar a informacao base noutro ficheiro.");
            System.out.println("e) Ordenar a informacao, alfabeticamente, pelo codigo da maquina.");
            System.out.println("f) Ordenar a informacao, decrescentemente, pela produtividade semanal.");
            System.out.println("g) Listar no ecra toda a informacao existente em memoria.");
            System.out.println("h) Listar as maquinas cuja producao semanal foi inferior a 30% da sua capacidade produtiva semanal."); 
            System.out.println("i) Inserir a produtividade das maquinas num determinado dia da semana.");
            System.out.println("j) Determinar, para cada maquina, o(s) dia(s) da semana em que o total de horas de producao foi maior e menor.");
            System.out.println("k) Imprimir, num ficheiro de texto, a producao semanal por tipo de maquina.");
            System.out.println("l) Guardar toda a informacao existente em memoria num ficheiro de texto.");
            System.out.println("m) Melhorar a eficiencia da funcionalidade do ponto 7 (informacao do ficheiro ordenada pelo codigo da maquina).");
            System.out.println("n) Guardar o caminho onde os ficheiros estao guardados.");
            System.out.println("o) Sair.");
            System.out.println("");

            System.out.print("Introduza a opção: ");
            opcao = scanner.next().charAt(0);
            LimparConsola limpar = new LimparConsola();
        
        
            verificar = false;
            limpar.limpar();
            switch (opcao) {
                case 'a':
                    ver.mostrarFicheiro();
                    break;
                case 'b':
                    AtualizarCapProdDiariaMaq atualizar = new AtualizarCapProdDiariaMaq();
                    atualizar.codMaquinaCapDiaria();
                    break;
                case 'c':
                    ver.eliminarmaquina();
                    break;
                case 'd':
                    ver.guardarInfo();
                    break;
                case 'e':
                    ver.CodOrdemAlfabetica();
                    break;
                case 'f':
                    Maquina ordDesc = new Maquina();
                    ordDesc.lerFicheiro();
                    ordDesc.CodOrdemDecrescenteProdSemanal();
                    break;
                case 'g':
                    ver.listarInfo();
                    break;
                case 'h':
                    semana.producaoDiasDaSemana();
                    break;
                case 'i':
                    InserirProdMaqDiaSemana insProdSemanal = new InserirProdMaqDiaSemana();
                    insProdSemanal.InsProdMaqDiaSemana();
                    break;
                case 'j':
                    Maquina maiormenor=new Maquina();
                    maiormenor.producaoMaiorMenor();
                    break;
                case 'k':
                    ver.listagemProducaoSemanal();
                    break;
                case 'l':
                    semana.gravarInformacaoArrayProducao();
                    break;
                case 'm':
                    System.out.println("14+14");
                    break;
                case 'n':
                    ver.savePath();
                    break;
                case 'o':
                    System.out.println("Saiu com sucesso");
                    verificar = true;
                    break;
                default:
                    System.out.println("Opção invalida");
                    System.out.println("Introduza a opção: ");
                    opcao = scanner.next().charAt(0);
                    verificar = true;
                    break;
            }
        } while (!verificar);
    }

    public static int retornaValorInteiro(String valor) {
        int valInteiro = 0;

        try {
            valInteiro = Integer.parseInt(valor);
        } catch (NumberFormatException e) {
            //System.out.println("****Erro na funcao retornar valor inteiro"); 
        }
        return valInteiro;
    }
}
