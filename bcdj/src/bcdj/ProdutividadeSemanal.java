/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bcdj;

import java.io.*;
import java.util.*;

/**
 *
 * @author PC
 */
public class ProdutividadeSemanal {

    Scanner scanner = new Scanner(System.in);
    String ficheiroMaq = "listaMaqInferior.txt";
    String fichGravar = "producao.txt";

    String[][] lista = new String[80][2];
    String[] diasSemana = {"Segunda", "Terca", "Quarta", "Quinta", "Sexta", "Sabado", "Domingo"};
    //criar um array que armazene as informacoes da maquina e da producao de cada uma delas em cada dia da semana
    String[][] arrayProducao;
    int nlinhas;
    
    /*--
        String nomeFich
    */
    public void capacidadeProdutivaSemanal(String nomeFich) {
        Maquina ver = new Maquina();
        ver.lerFicheiro();
    }
    
    /*--
        Vai armazenar as maquinas cuja producao semanal foi menor do que 30%
    */
    public void producaoDiasDaSemana() {
        try {

            //chamar classe Maquina
            Maquina prodDiaria = new Maquina();
            //metodo de ler ficheiro
            prodDiaria.lerFicheiro();
            // declarar o tamanho do vetor 
            arrayProducao = new String[prodDiaria.getLista().length][9];
            int row = 0;

            //vai ver linha-a-linha 
            for (String[] linha : prodDiaria.getLista()) {
                arrayProducao[row][0] = linha[0];
                arrayProducao[row][1] = linha[1];
                row++;
            }

            //percorrer todos os dias da semana, de modo a poder ler todos os ficheiros 
            for (int i = 0; i < diasSemana.length; i++) {
                Maquina prodDia = new Maquina();
                prodDia.lerFicheiro(diasSemana[i] + ".txt");

                //ler linha-a-linha do arrayProducao
                for (String[] linha : arrayProducao) {
                    //ler linha-a-linha do ficheiro do dia da semana
                    for (String[] linhaDia : prodDia.getLista()) {
                        //verificar se o codigo da maquina do arrayProducao existe no ficheiro do dia da semana
                        if (linha[0].equals(linhaDia[0])) {
                            //preenche a capacidade de producao de cada dia da semana 
                            linha[i + 2] = linhaDia[1];

                        }
                    }
                }
            }

            //criar uma lista para armazenar as maquinas cuja producao semanal foi menor que 30% da capacidade produtiva semanal
            ArrayList<String> listaLinhas = new ArrayList<>();
            //ler linha-a-linha do array producao
            for (String[] linha : arrayProducao) {
                int capProdSemanal, producaoSemanal;
                //calculo da capacidade produtiva semanal de cada maquina
                capProdSemanal = 7 * Bcdj.retornaValorInteiro(linha[1]);
                //calculo da producao semanal de cada maquina 
                producaoSemanal = Bcdj.retornaValorInteiro(linha[2]) + Bcdj.retornaValorInteiro(linha[3]) + Bcdj.retornaValorInteiro(linha[4]) + Bcdj.retornaValorInteiro(linha[5]) + Bcdj.retornaValorInteiro(linha[6]) + Bcdj.retornaValorInteiro(linha[7]) + Bcdj.retornaValorInteiro(linha[8]);

                //condicao para distinguir as maquinas que devem ser adicionadas a lista criada 
                if (producaoSemanal < (0.3 * capProdSemanal)) {
                    //adiciona na lista, o codigo das maquinas cuja condicao acima e verdadeira
                    listaLinhas.add(linha[0]);
                }
            }
            //chama o metodo listarMaqInferior para gravar as maquinas no ficheiro  
            listarMaqInferior(listaLinhas);
            System.out.println("A lista das maquinas foi gravada com sucesso.");
        } catch (IOException e) {

        }
    }

    //metodo para gravar a lista das maquinas com produtividade semanal inferior a 30% da sua capacidade produtiva diaria 
    /*--
        lista
        Vai listar as maquinas com uma produtividade semanal inferior a 30%
    */
    public void listarMaqInferior(ArrayList<String> lista) throws FileNotFoundException, IOException {
        try {
            //ficheiro onde a lista vai ser gravada
            BufferedWriter f = new BufferedWriter(new FileWriter(ficheiroMaq));
            for (int i = 0; i < lista.size(); i++) {
                f.write(lista.get(i));
                f.newLine();
            }
            f.close();
        } catch (FileNotFoundException ex) {

        }
    }
    
    //metodo para gravar toda a informacao existente em memoria num ficheiro de texto
    /*--
        Vai gravar a informação no arrayProducao
    */
    public void gravarInformacaoArrayProducao() {
        //gravar informacao no arrayProducao
        //este array so e preenchido quando o metodo producaoDiasDaSemana é executado 
        if (arrayProducao != null) {
            try {
                BufferedWriter f = new BufferedWriter(new FileWriter(fichGravar));
                for (int i = 0; i < arrayProducao.length; i++) {
                    f.write(arrayProducao[i][0] + "," + arrayProducao[i][1] + "," + arrayProducao[i][2] + "," + arrayProducao[i][3] + "," + arrayProducao[i][4] + "," + arrayProducao[i][5] + "," + arrayProducao[i][6] + "," + arrayProducao[i][7] + "," + arrayProducao[i][8]);
                    f.newLine();
                }
                f.close();
            } catch (IOException ex) {
                System.out.println("Informacao gravada com sucesso!");
            }
        }
    }
}
