package bcdj;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class InserirProdMaqDiaSemana {
    String diaSemana;
    Scanner scanner = new Scanner(System.in);
    String[][] lista = new String[80][2];
    private File ficheiro = null;
    private BufferedReader  r = null;
    int nlinhas;
    int row = 0;
    String line = "";
        
    /*--
   Permite inserir a produtividade das máquinas num determinado
    dia da semana, através do código da máquina e da
    respectiva produtividade.
    */
    public void InsProdMaqDiaSemana() {
        
        boolean adp = true;
        boolean adpt = true;
        boolean adptd = true;
        int opcao=0;
        String codMaquina="";
        int capProdDiaria=0;
        
        while (adp) {
        
            // Pedir o dia da semana para ir buscar esse ficheiro
            
            System.out.println("1) Domingo");
            System.out.println("2) Segunda");
            System.out.println("3) Terça");
            System.out.println("4) Quarta");
            System.out.println("5) Quinta");
            System.out.println("6) Sexta");
            System.out.println("7) Sábado");
            System.out.println("");
            
            System.out.println("Introduza o dia:");
            opcao = scanner.nextInt();
        
        // vai verificar o dia da semana introduzido, se for invalido volta ao ponto acima
        if (opcao == 1) {
            diaSemana = "domingo.txt";
            adp = false;
            break;
        } else if (opcao == 2) {
            diaSemana = "segunda.txt";
            adp = false;
            break;
        } else if (opcao == 3) {
            diaSemana = "terca.txt";
            adp = false;
            break;
        } else if (opcao == 4) {
            diaSemana = "quarta.txt";
            adp = false;
            break;
        } else if (opcao == 5) {
            diaSemana = "quinta.txt";
            adp = false;
            break;
        } else if (opcao == 6) {
            diaSemana = "sexta.txt";
            adp = false;
            break;
        } else if (opcao == 7) {
            diaSemana = "sabado.txt";
            adp = false;
            break;
        }
        
        if (adp) {
            System.out.println("");
            System.out.println("Dia da Semana Invalido!");
            System.out.println("");
        }

    }
        
        try {
            // Vai ler o ficheiro indicado
            Boolean sair = false;
            ficheiro = new File(diaSemana);
            r = new BufferedReader(new FileReader(ficheiro));
            //criação de uma lista para identificar o nº de linhas que existe no ficheiro 
            ArrayList<String> listaLinhas = new ArrayList<String>();
            //vai ler o ficheiro linha-a-linha e vai adicionar as linhas no ficheiro 
            while (!sair) {
                line = r.readLine();
                if (line != null) {
                    listaLinhas.add(line);
                } else {
                    sair = true;
                }
                //verificação para não ultrapassar as 80 linhas 
                if (listaLinhas.size() == 80) {
                    sair = true;
                }
            }
             nlinhas = listaLinhas.size();
            //definir o tamanho da lista 
            lista = new String[listaLinhas.size()][2];
            //
            String[] arrayLinha = new String[2];
            //vai linha-a-linha da lista 
            for (String linha : listaLinhas) {
                //cada linha tem uma string que vai está dividida em duas posições, entre vírgulas
                arrayLinha = linha.split(",");
                lista[row][0] = arrayLinha[0];
                lista[row][1] = arrayLinha[1];
                row++;
            }

            r.close();
        } catch (IOException | NumberFormatException e) {
            //se der erro sai do método
            System.out.println("Não conseguiu ler o ficheiro!");
    }
        int i = 0;
        String[] linha;
        
        // Vai escrever na matriz a informacao do ficheiro
        try {
            Scanner in = new Scanner(new File(diaSemana));
            while (in.hasNextLine() && i < 80) {
                linha = in.nextLine().split(",");
                lista[i][0] = linha[0];
                lista[i][1] = linha[1];
                i++;
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        }
        
        int j = 0;
        
        // Vai mostrar no ecra
        for (j = 0; j < lista.length; j++) {
                System.out.print(lista[j][0]+","+lista[j][1]);
            System.out.println();
        }
        
        // Vai inserir a capacidade produtiva diária, para a maquina indicada, até digitar 000 
        while (adpt) {
        
        System.out.println("");
        System.out.println("Digite o codigo da maquina que vai ser alterado: (000 Para sair)");
        codMaquina = scanner.next();
        System.out.println("");
        
        for (int k = 0; k < j; k++) {
            if (codMaquina.equals(lista[k][0])) {
                while (adptd) {
                    System.out.println("Digite a capacidade produtiva diária a inserir:");
                    capProdDiaria = scanner.nextInt();
                    
                    if (capProdDiaria < 24) {
                        adptd = false;
                        break;
                    }
                    
                }
            }
            adptd = true;
        }
        
        
        // Se o codigo da maquina inserido for 000 vai sair do ciclo while
        if ("000".equals(codMaquina)) {
            adpt = false;
        }
        
        // Se o codigo maquina introzido for igual, a algum da matriz "lista", então a capacidade vai ser alterada
        for (int k = 0; k < j; k++) {
            if (codMaquina.equals(lista[k][0])) {
                lista[k][1] = Integer.toString(capProdDiaria);
            }
        }

            String eol = System.getProperty("line.separator");
            
            BufferedWriter bw = null;
            
            // Vai escrever no ficheiro
            try {
                String[][] conteudo = new String[j][2];
                
                for (int k = 0; k < j; k++) {
                    for (int l = 0; l < 2; l++) {
                        conteudo[k][l] = lista[k][l];
                    }
                }
                
                //Nome do caminho e arquivo (Neste caso colocamos na pasta do programa)
                File file = new File(diaSemana);

                /* Cria o ficheiro caso o ficheiro não exista*/
                if (!file.exists()) {
                    file.createNewFile();
                }
            // Vai escrever no ficheiro
            FileWriter fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            for (int k = 0; k < i; k++) {
                for (int l = 0; l < 1; l++)
                    bw.write(conteudo[k][l] + "," + conteudo[k][l+1] + "\r\n");
            }
            
            } catch (IOException ioe) {
               ioe.printStackTrace();
            }
            finally
            { 
               try{
                  if(bw!=null)
                     bw.close();
               }catch(Exception ex){
                   System.out.println("Erro ao atualizar"+ex);
                }
            }
        
        }
        
        /*--
        Recupera para a memória a informação guardada no ficheiro 
        */
        int recuperar;
        int fechar;
        boolean adptdr = true;
        
        // Vai perguntar se pretende mostrar no ecra o que está em memória
        System.out.println("Pretende Recuperar para a memória a infomação guardada?");
        System.out.println("1) Sim");
        System.out.println("2) Não");
        
        System.out.println("Introduza a opção:");
        recuperar = scanner.nextInt();
        
        // Vai listar a informação em memória, até que seja digitado pelo utilizador um 0
        if (recuperar == 1) {
            do {
                for (int k = 0; k < lista.length; k++) {
                    System.out.print(lista[k][0]+","+lista[k][1]);
                System.out.println();
                }

                System.out.println("");
                System.out.println("Prima 0 para sair");
                fechar = scanner.nextInt();

                if (fechar == 0) {
                    adptdr = false;
                }
                } while (adptdr == true);
        }
        
    }
    
}
