package bcdj;

import java.io.*;
import java.util.*;

/**
 * Classe Maquina - contempladas todas as informacoes e todos os métodos
 * relacionados com a linha de producao, mais concretamente, com as maquinas
 * dessa linha.
 *
 * @authores bruno barbosa, claudia nunes, daniel fonseca, joao leal
 */
public class Maquina {

    String[][] lista;

    // retorno da lista (propriedade)
    /*--
        lista
     */
    public String[][] getLista() {
        return this.lista;
    }

    //declaracao de variaveis
    Scanner ler = new Scanner(System.in);
    String nomeFich = "maquinas.txt";
    int nlinhas;
    String codigo;
    private File ficheiro = null;
    private BufferedReader r = null;
    String diasdasemana[] = {"Segunda", "Terca", "Quarta", "Quinta", "Sexta", "Sabado", "Domingo"};

    // metodo para ler o ficheiro maquinas.txt por defeito 
    /*--
        lerFicheiro
     */
    public void lerFicheiro() {
        lerFicheiro(nomeFich);
    }

    /*--
        String nomeFicheiro
    
        Vai ler o ficheiro maquinas.txt e inserir na matriz lista
     */
    public void lerFicheiro(String nomeFicheiro) {
        String line = "";
        int row = 0;

        try {
            Boolean sair = false;
            ficheiro = new File(nomeFicheiro);
            r = new BufferedReader(new FileReader(ficheiro));
            //criação de uma lista para identificar o nº de linhas que existe no ficheiro 
            ArrayList<String> listaLinhas = new ArrayList<String>();
            //vai ler o ficheiro linha-a-linha e vai adicionar as linhas no ficheiro 
            while (!sair) {
                line = r.readLine();
                if (line != null) {

                    listaLinhas.add(line);
                } else {
                    sair = true;
                }
                //verificação para não ultrapassar as 80 linhas 
                if (listaLinhas.size() == 80) {
                    sair = true;
                }
            }
            nlinhas = listaLinhas.size();
            //definir o tamanho da lista 
            lista = new String[listaLinhas.size()][2];
            //
            String[] arrayLinha = new String[2];
            //vai linha-a-linha da lista 
            for (String linha : listaLinhas) {
                //cada linha tem uma string que vai está dividida em duas posições, entre vírgulas
                arrayLinha = linha.split(",");
                lista[row][0] = arrayLinha[0];
                lista[row][1] = arrayLinha[1];
                row++;
            }
            r.close();
        } catch (IOException | NumberFormatException e) {
            //se der erro sai do método
            System.out.println("Não conseguiu ler o ficheiro!");
        }
    }

    //metodo para mostrar no ecran a informacao base de uma determinada maquina e cujo codigo e introduzido pelo teclado
    /*--
        Vai mostrar no ecra a capacidade produtiva diaria do codigo da maquina introduzido
     */
    public void mostrarFicheiro() {
        boolean adp = true;
        while (adp) {
            String codigo;
            //pedir código da máquina a qual se pretende saber toda a informação 
            System.out.println("Introduza o código da máquina que pretende visualizar:");
            //ler código introduzido
            codigo = ler.next();

            for (String[] linha : lista) {
                if (codigo.equals(linha[0])) {
                    System.out.println("A máquina tem código " + linha[0] + " e a sua capacidade produtiva diária é " + linha[1] + " horas.");
                    adp = false;
                    break;
                }
            }
            if (adp) {
                System.out.println("Codigo Invalido!");
            }
        }
    }

    //metodo para ordenar alfabeticamente toda a informacao que reside em memoria, organizada por codigo da maquina 
    /*--
        Vai ordenar por ordem alfabetica ascendente, por codigo da maquina
     */
    public void CodOrdemAlfabetica() {
        //ordenar, por ordem alfabetica 
        Arrays.sort(lista, (final String[] entrada1, final String[] entrada2) -> {
            final String maquina1 = entrada1[0];
            final String maquina2 = entrada2[0];
            return maquina1.compareTo(maquina2);
        });

        for (String[] linha : lista) {
            System.out.println(linha[0]);
        }
    }

    /*--
    Vai Ordenar toda a informação em memória organizada por ordem 
    decrescente da produtividade semanal.
     */
    public void CodOrdemDecrescenteProdSemanal() {
        Arrays.sort(lista, (final String[] entrada1, final String[] entrada2) -> {
            final Integer maquina1 = Integer.parseInt(entrada1[1]);
            final Integer maquina2 = Integer.parseInt(entrada2[1]);
            return -maquina1.compareTo(maquina2);
        });

        for (String[] linha : lista) {
            System.out.println(linha[0] + "," + linha[1]);
        }
    }

    /*--
     *   Metodo para gravar a informação base (codigo e capacidade produtiva da maquina) num ficheiro de texto
     */
    public void guardarInfo() throws FileNotFoundException, IOException {
        // Nao mudaria nada neste metodo
        try {
            BufferedWriter ficheiro = new BufferedWriter(new FileWriter(nomeFich));
            for (int row = 0; row < nlinhas; row++) {
                ficheiro.write(lista[row][0] + "," + lista[row][1]);
                ficheiro.newLine();

            }
            ficheiro.close();
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        }
        System.out.println("Dados em memória gravados no ficheiro!");
    }

    /*--
        Metodo para listar no ecra toda a informaçao existente em memoria
     */
    public void listarInfo() throws FileNotFoundException, IOException {
        for (int i = 0; i < nlinhas && lista[i][0] != null; i++) {
            System.out.println(lista[i][0] + "," + lista[i][1]);
        }
    }

    /**
     * Metodo para remover toda a informacao referente a maquina retirada da
     * producao, cujo codigo e introduzido via teclado
     */
    public void eliminarmaquina() {
        int cont = 0;
        String[][] listatmp = new String[80][2];
        System.out.println("Introduza o codigo da maquina que pretende eliminar");
        codigo = ler.next();
        for (int i = 0; i < nlinhas; i++) {
            if (codigo.equals(lista[i][0])) {
                System.out.println(lista[i][0] + "," + lista[i][1] + " Maquina eliminada");
            } else {

                listatmp[cont][0] = lista[i][0];
                listatmp[cont][1] = lista[i][1];
                cont++;
            }
        }
        lista = listatmp;
        for (int i = 0; i < 80 && lista[i][0] != null; i++) {
            System.out.println(lista[i][0] + "," + lista[i][1]);
        }
    }

    /**
     * Método para retornar valor inteiro de uma string
     *
     * @param valor - String a converter para valor inteiro
     * @return inteiro - valor inteiro
     */
    public static int retornaValorInteiro(String valor) {
        int valInteiro = 0;

        try {
            valInteiro = Integer.parseInt(valor);
        } catch (NumberFormatException e) {
            //System.out.println("****Erro na funcao retornar valor inteiro"); 
        }
        return valInteiro;

    }

    /**
     * Metodo para determinar, para cada maquina, o(s) dia(s) da semana com
     * maior e menor numero total de horas de producao
     */
    //Nota: relativamente a este metodo nao faria nada de diferente.
    public void producaoMaiorMenor() {
        //criar um array que armazene as informacoes da maquina e da producao de cada uma delas em cada dia da semana
        String[][] arrayProducao;
        //chamar classe Maquina
        Maquina prodDiaria = new Maquina();
        //metodo de ler ficheiro
        prodDiaria.lerFicheiro();
        // declarar o tamanho do vetor 
        arrayProducao = new String[prodDiaria.getLista().length][9];
        int row = 0;

        //vai ver linha-a-linha 
        for (String[] linha : prodDiaria.getLista()) {
            arrayProducao[row][0] = linha[0];

            row++;
        }

        //percorrer todos os dias da semana, de modo a poder ler todos os ficheiros 
        for (int i = 0; i < diasdasemana.length; i++) {
            Maquina prodDia = new Maquina();
            prodDia.lerFicheiro(diasdasemana[i] + ".txt");
            //ler linha-a-linha do arrayProducao
            for (String[] linha : arrayProducao) {
                //ler linha-a-linha do ficheiro do dia da semana
                for (String[] linhaDia : prodDia.getLista()) {
                    //verificar se o codigo da maquina do arrayProducao existe no ficheiro do dia da semana
                    if (linha[0].equals(linhaDia[0])) {
                        //preenche a capacidade de producao de cada dia da semana 
                        linha[i + 2] = linhaDia[1];

                    }
                }
            }
        }

        //ler linha-a-linha do array producao
        for (String[] linha : arrayProducao) {
            int menor = 500, maior = 0;
            String maiordiasemana = "", menordiasemana = "";

            for (int i = 0; i < 7; i++) {
                if (Maquina.retornaValorInteiro(linha[2 + i]) > maior) {
                    maior = Maquina.retornaValorInteiro(linha[2 + i]);
                    maiordiasemana = diasdasemana[i] + ", ";
                } else if (Maquina.retornaValorInteiro(linha[2 + i]) == maior) {
                    maiordiasemana += diasdasemana[i] + ", ";
                }

            }

            for (int i = 0; i < 7; i++) {
                if (Maquina.retornaValorInteiro(linha[i]) < menor) {
                    menor = Maquina.retornaValorInteiro(linha[2 + i]);
                    menordiasemana = diasdasemana[i] + ", ";
                } else if (Maquina.retornaValorInteiro(linha[i]) == menor) {
                    menordiasemana += diasdasemana[i] + ", ";
                }

            }

            System.out.println("Codigo- " + linha[0]);
            System.out.println(" Maior dia- " + maiordiasemana + " Menor dia- " + menordiasemana);
        }

    }

    /*--
      Metodo para imprimir num ficheiro de texto a produçao semanal, por tipo de maquina
     */
    public void listagemProducaoSemanal() {
        String tipo = "";
        int[] totais = new int[3];
        do {
            System.out.println("Introduzir tipo da maquina:");
            try {
                tipo = ler.next();
            } catch (Exception e) {
                System.out.println("Erro string invalida");
                tipo = "";
            }
        } while (tipo.equals(""));
        tipo = tipo.toUpperCase();
        System.out.format("%30s%30s%30s%30s", "Código Máquina", "Capac. Prod. Sem.", "Prod. Semanal", "Desvio Produção");
        System.out.println("");
        System.out.println("==================================================================================================================================================================================================");
        /*for (int i = 0; i < arrayProducao.length && arrayProducao[i] != null; i++) {
            if (tipo.equals(arrayProducao[i][0].substring(0, 2))) {
                System.out.format("%30s%30d%30d%30s", arrayProducao[i][0], arrayProducao[i][1] * 7, arrayProducao[i][2], Integer.toString(arrayProducao[i][] - arrayProducao[i].[] * 7) + " horas");
                System.out.println("");
                totais[0] += arrayProducao[i][] * 7;
                totais[1] += arrayProducao[i][]();
                totais[2] += arrayProducao[i][]() - arrayProducao[i][] * 7;
            }
        }*/
        System.out.println("==================================================================================================================================================================================================");
        System.out.format("%30s%30d%30d%30s", "", totais[0], totais[1], totais[2] + " horas");
    }

    /*--
         Metodo para guardar o caminho onde os ficheiros estao guardados , num ficheiro com o nome config.txt
     */

    public void savePath() throws FileNotFoundException, IOException {
        String pasta, filepath;
        System.out.println("Escreva o nome da pasta onde os ficheiros estão guardados");
        pasta = ler.next();
        filepath = ("/" + pasta + "/");
        BufferedWriter ficheiro = new BufferedWriter(new FileWriter("config.txt"));
        ficheiro.write(filepath);
        ficheiro.newLine();
        ficheiro.close();
    }

}
